#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include <iterator>
#include <errno.h>
#include <chrono>
#include <ctime>
#include <sys/types.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <libgen.h>
#include <fcntl.h>
#include <limits.h>
#include <mutex>
#include <vector>
#include <string.h>
#include <unistd.h>
#include <execinfo.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <syslog.h>
#include <thread>
#define MAX_EVENTS 1024                                /*Максимальное кличество событий для обработки за один раз*/
#define LEN_NAME 128                                   /*Будем считать, что длина имени файла не превышает 16 символов*/
#define EVENT_SIZE (sizeof(struct inotify_event))      /*размер структуры события*/
#define BUF_LEN (MAX_EVENTS * (EVENT_SIZE + LEN_NAME)) /*буфер для хранения данных о событиях*/
#define CHILD_NEED_WORK 100
#define FD_LIMIT 512
#define CHILD_NEED_TERMINATE 101
#define PROG_NAME "auditdaemon"
/* Файл журнала*/

char logname[256] = "";
char dlogname[256] = "";
char configname[256] = "";
char rootdir[256] = "";

std::mutex threadMutex;
volatile bool daemonActive;
volatile int fd;

struct watchedFile
{
    char name[256] = "";
    char dir[256] = "";
    int wd;
};

std::vector<watchedFile> files;

void loadConfig();
void reloadConfig();
void writePID();

watchedFile parseFile(std::string path);

void stopHandler(int sig)
{
    syslog(LOG_NOTICE, "Got exit signal");
    threadMutex.lock();
    daemonActive = false;
    threadMutex.unlock();
}

void addWatchesToFile(watchedFile &file)
{

    int wd = inotify_add_watch(fd, file.dir, IN_ALL_EVENTS);
    if (wd == -1)
    {
        char s[256] = ""; // initialized properly
        sprintf(s, "Couldn't add watch to %s/%s\n", file.dir, file.name);
        syslog(LOG_NOTICE, s);
    }
    else
    {
        file.wd = wd;
        char s[256] = ""; // initialized properly
        sprintf(s, "Watching file %s/%s\n", file.dir, file.name);
        syslog(LOG_NOTICE, s);
    }
}

void rmWatchesFromFile(watchedFile &file)
{
    int wd = inotify_rm_watch(fd, file.wd);
}

void printEventType(inotify_event *event, char *type, char *logfile)
{
    char *fileType;

    if (event->mask & IN_ISDIR)
        fileType = "Dir";
    else
        fileType = "File";
    //  FILE *nn = fopen(logfile,"w");
    // if(nn!=0)
    const std::time_t tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::ofstream fout;
    fout.open(logfile, std::ios::app);
    fout  << fileType << "\t" << event->name << "\t" << type <<"\t" << ctime(&tt);
    fout.close();

//    char s[256] = "";
//    sprintf(s, "%s %s %s", fileType, event->name, type);
//    syslog(LOG_NOTICE, s);
}

void printEvent(inotify_event *event, char *logfile)
{
    if (event->mask & IN_CREATE)
    {
        printEventType(event, "Create", logfile);
    }
    if (event->mask & IN_ACCESS)
    {
        printEventType(event, "Access", logfile);
    }
    if (event->mask & IN_OPEN)
    {
        printEventType(event, "Open", logfile);
    }
    if (event->mask & IN_MODIFY)
    {
        printEventType(event, "Modify", logfile);
    }
    if (event->mask & IN_DELETE)
    {
        printEventType(event, "Delete", logfile);
    }
    if (event->mask & IN_ATTRIB)
    {
        printEventType(event, "Attribute changed", logfile);
    }
    if (event->mask & IN_CLOSE_WRITE)
    {
        printEventType(event, "Close write", logfile);
    }
    if (event->mask & IN_CLOSE_NOWRITE)
    {
        printEventType(event, "Close nowrite", logfile);
    }
    if (event->mask & IN_MOVE_SELF)
    {
        printEventType(event, "Move self", logfile);
    }
    if (event->mask & IN_MOVED_FROM)
    {
        printEventType(event, "Moved from", logfile);
    }
    if (event->mask & IN_MOVED_TO)
    {
        printEventType(event, "Moved to", logfile);
    }
}

void addConfigToWatched()
{
    files.push_back(parseFile(std::string(configname)));
    syslog(LOG_NOTICE, "Config added to watched");
}

bool seekWatched(char *name)
{
    for (watchedFile n : files)
    {
        if (strcmp(n.name, name) == 0)
            return true;
    }
    return false;
}
void handleEvents()
{
    syslog(LOG_NOTICE, "Entered work thread");
    bool da;
    char buffer[BUF_LEN];
    da = daemonActive;
    while (da)
    {
      //  threadMutex.lock();
        da = daemonActive;
       // threadMutex.unlock();
        int i = 0;
        int length = read(fd, buffer, BUF_LEN);
        //  printf( "length %d\n",length);
        while (i < length)
        {
           
            struct inotify_event *event = (struct inotify_event *)&buffer[i];
             
            //	printf( "New event len %d\n",event->len);
            if (!event->len || !da)
                break;
          //  syslog(LOG_NOTICE, event->name);
            i += EVENT_SIZE + event->len;
            if (strcmp(event->name, "config.cfg") == 0)
            {
                if (event->mask & IN_CLOSE_WRITE)
                {
                    reloadConfig();
                }
                continue;
            }
            if (seekWatched(event->name))
            {
                printEvent(event, logname);
            }
        }
    }
}

int auditDaemon(char *conf, char *log)
{
    sigset_t sigset;
    int signo;
    sigaddset(&sigset, SIGUSR1);
    sigprocmask(SIG_BLOCK, &sigset, NULL);

    fd = inotify_init();
    if(fd<0)
    {
        syslog(LOG_NOTICE, "inotify init error");
        return 0;
    }
    strcpy(configname, conf);
    strcpy(logname, log);
    char pidpath[256] = "";
    watchedFile config = parseFile(std::string(configname));
    strcpy(pidpath, config.dir);

    int wd = inotify_add_watch(fd, config.dir, IN_CLOSE_WRITE);
    if(fd<0)
    {
        syslog(LOG_NOTICE, "inotify add config error");
        return 0;
    }
    std::ofstream out(strcat(pidpath, "/auditdaemon.pid"));
    out << getpid() << std::endl;
    out.close();
    char s[256] = ""; // initialized properly
    sprintf(s, "Cfg \t %s\nLog \t%s", configname, logname);
    syslog(LOG_NOTICE, s);
    loadConfig();

   // threadMutex.lock();
    daemonActive = true;
    //threadMutex.unlock();
    // *da=true;
    std::thread wt = std::thread(handleEvents);

    while (1)
    {
        sigwait(&sigset, &signo);
        if (signo == SIGTERM)
        {
        //    threadMutex.lock();
            daemonActive = false;
         //   threadMutex.unlock();
            syslog(LOG_NOTICE, "Update signal detected");
            wt.join();
            return 0;
        }
    }
    return 0;
}

int readPID(char *path)
{
    int pid;
    std::ifstream in(path, std::ios::in);
    in >> pid;
    in.close();
    return pid;
}
static void skeleton_daemon()
{
    pid_t pid;

    /* Fork off the parent process */
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* On success: The child process becomes session leader */
    if (setsid() < 0)
        exit(EXIT_FAILURE);

    /* Catch, ignore and handle signals */
    //TODO: Implement a working signal handler */

    /* Fork off for the second time*/
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* Set new file permissions */
    umask(0);

    /* Change the working directory to the root directory */
    /* or another appropriated directory */
    chdir("/");

    /* Close all open file descriptors */
    int x;
    for (x = sysconf(_SC_OPEN_MAX); x >= 0; x--)
    {
        close(x);
    }

    /* Open the log file */
    openlog(PROG_NAME, LOG_PID, LOG_DAEMON);
}
/**
    Keys:
    -c [filename] - config file
    -l [filename] - logfile name
    -n [filename] - new file add to watch
*/

void addToConfig(char *configname, char *newfile)
{
    std::ofstream fout;
    fout.open(configname, std::ios::app);
    fout << newfile << std::endl;
    fout.close();
    printf("NewFile added: %s\n", newfile);
}

watchedFile parseFile(std::string path)
{

    char seperator = '/';
    std::size_t sepPos = path.rfind(seperator);
    std::string name = path.substr(sepPos + 1, path.size() - 1);
    std::string dir = path.substr(0, sepPos);
    struct watchedFile temp;
    strcpy(temp.name, name.c_str());
    strcpy(temp.dir, dir.c_str());
    return temp;
}

void loadConfig()
{
    std::string str;
    std::ifstream in(configname, std::ios::in);
    while (getline(in, str))
    {
        files.push_back(parseFile(str));
    }
    for (watchedFile n : files)
    {
        addWatchesToFile(n);
    }
    in.close();
    syslog(LOG_NOTICE, "Config loaded");
}
void clearFiles()
{
    syslog(LOG_NOTICE, "Clearing watched files");
    for (watchedFile n : files)
        rmWatchesFromFile(n);
    files.clear();
    syslog(LOG_NOTICE, "Files cleared");
}

void reloadConfig()
{
    syslog(LOG_NOTICE, "Reloading config");
    clearFiles();
    loadConfig();
    syslog(LOG_NOTICE, "Config reloaded");
}
bool removeFromConfig(size_t index)
{

   // rmWatchesFromFile(files[index]);
    std::vector<std::string> vec;
    std::ifstream file(configname);
    if (file.is_open())
    {
        std::string str;
        while (std::getline(file, str))
                vec.push_back(str);
        file.close();
        if (vec.size() < index)
            return false;
        std::cout<<"Removed file: "+(vec[index-1])<<std::endl;
        vec.erase(vec.begin() + index - 1);
        std::ofstream outfile(configname);
        if (outfile.is_open())
        {
            std::copy(vec.begin(), vec.end(),
                      std::ostream_iterator<std::string>(outfile, "\n"));
            outfile.close();
            return true;
        }
        return false;
    }
    return false;
}

void startDaemon(char *conf, char *log)
{
    skeleton_daemon();
    syslog(LOG_NOTICE, "Daemon started");
    auditDaemon(conf, log);
    syslog(LOG_NOTICE, "daemon  terminated");
    closelog();
}

void printConfig()
{
    std::string str;
    std::ifstream in(configname, std::ios::in);
    int cntr = 1;
    while (getline(in, str))
    {
        std::cout << cntr++ << ")" << str << std::endl;
    }
    in.close();
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        printf("Wrong arguments\n");
        return -1;
    }
    strcpy(configname, argv[1]);
    strcpy(logname, argv[2]);
    bool daemon = false;
    for (int i = 3; i < argc; i++)
    {
        if (strcmp(argv[i], "--run") == 0)
        {
            syslog(LOG_NOTICE, "entered run option");
            startDaemon(argv[1], argv[2]);
            return 0;
        }
        if (strcmp(argv[i], "--add") == 0)
        {
            if (i == argc)
            {
                return -1;
            }
            addToConfig(configname, argv[++i]);
            return 0;
        }
        if (strcmp(argv[i], "--stop") == 0)
        {
            if (i == argc)
            {
                return -1;
            }
            //   printf("%s\n",argv[1]);
            char pidpath[256] = "";
            watchedFile config = parseFile(std::string(argv[1]));
            strcpy(pidpath, config.dir);
            // printf("%s\n",pidpath);
            strcat(pidpath, "/auditdaemon.pid");
            //  printf("%s\n",pidpath);
            int pid = readPID(pidpath);
            printf("%d\n", pid);
            kill(pid, SIGTERM);
            return 0;
        }
        if (strcmp(argv[i], "--lw") == 0)
        {
            if (i == argc)
            {
                return -1;
            }
            printConfig();
            return 0;
        }
        if (strcmp(argv[i], "--rm") == 0)
        {
            size_t index;

            if (i == argc)
            {
                return -1;
            }
            try
            {
                index = stoi(std::string(argv[++i]));
            }
            catch (const std::invalid_argument &ia)
            {
                printf("Argument is not a number");
                return -1;
            }
            printf("Removing file with index %d\n",index);
            removeFromConfig(index);
            return 0;
        }
    }
    return -1;
}
